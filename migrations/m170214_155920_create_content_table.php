<?php

use yii\db\Migration;

/**
 * Handles the creation of table `content`.
 */
class m170214_155920_create_content_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%content}}', [
            'content_id' => $this->primaryKey(),
            'article_id' => $this->integer()->notNull(),
            'title' => $this->string(255),
            'content' => $this->text(),
        ]);
        
        $this->createIndex('article_id_idx', '{{%content}}', 'article_id', true);
        
        $this->addForeignKey(
            'content_to_article_article_id_fk', 
            '{{%content}}',
            'article_id',
			'{{%article}}',
            'article_id'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {   
        $this->dropForeignKey('content_to_article_article_id_fk', '{{%content}}');
        
        $this->dropIndex('article_id_idx', '{{%content}}');
        
        $this->dropTable('{{%content}}');
    }
}

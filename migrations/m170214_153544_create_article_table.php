<?php

use yii\db\Migration;
//use yii\db\Schema;

/**
 * Handles the creation of table `article`.
 */
class m170214_153544_create_article_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%article}}', [
            'article_id' => $this->primaryKey(),
            'sort' => $this->smallInteger()->defaultValue(0)->notNull(),
            'public' => $this->boolean()->defaultValue(0)->notNull(),
            'public_at' => $this->timestamp()->defaultValue((new \yii\db\Expression('NOW()')))->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%article}}');
    }
}

<?php

use app\models\Article;
use app\models\Content;
use yii\db\Migration;

class m170214_170349_insert_articles_and_content extends Migration
{
    public function safeUp()
    {
        for ($i = 1; $i <= 100; $i++) {
            $article = new Article([
                'public' => true,
                'sort' => $i,
            ]);
            $article->save();
            
            $content = new Content([
                'article_id' => $article->article_id,
                'title' => 'Заголовок №' . $i,
                'content' => 'Текст №' . $i,
            ]);
            $content->save();
        }
    }

    public function safeDown()
    {
        $this->delete(Content::tableName());
        $this->delete(Article::tableName());
    }
}

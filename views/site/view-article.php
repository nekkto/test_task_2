<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Просмотр статьи';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <h2><?= Html::encode($model->content->title); ?></h2>

                <p><?= Html::encode($model->content->content); ?></p>
                
                <p><?= Html::a('Назад', Url::to(['site/index']), ['class' => 'btn btn-default']); ?></p>
            </div>
        </div>

    </div>
</div>

<?php

/* @var $this yii\web\View */
/* @var $searchForm app\models\forms\ArticleSearchForm */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Блог';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <h2>Статьи</h2>

                <?= GridView::widget([
                    'dataProvider' => $searchForm->search(),
                    'filterModel' => $searchForm,
                    'columns' => [
                        'article_id',
                        'sort',
                        'public_at',
                        [
                            'attribute' => 'title',
                            'content' => function ($model) {
                                return Html::a($model->content->title, Url::to(['site/view-article', 'article_id' => $model->article_id]));
                            }
                        ],
                    ],
                ]); ?>
            </div>
        </div>

    </div>
</div>

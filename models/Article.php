<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "article".
 *
 * @property integer $article_id
 * @property integer $sort
 * @property integer $public
 * @property string $public_at
 *
 * @property Content $content
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sort', 'public'], 'integer'],
            [['public_at'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'article_id' => 'ID статьи',
            'sort' => 'Порядок',
            'public' => 'Видна всем',
            'title' => 'Заголовок',
            'public_at' => 'Опубликовано',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContent()
    {
        return $this->hasOne(Content::className(), ['article_id' => 'article_id']);
    }
    
    public function afterFind()
    {
        parent::afterFind();
        
        //foreach (['content_id', 'title', 'content'] as $atr) {
        //    $this->{$atr} = $this->contentRecord->{$atr};
        //}
    }
}

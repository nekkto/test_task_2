<?php

namespace app\models\forms;

use app\models\Article;
use app\models\Content;

use Yii;
use yii\data\ActiveDataProvider;

class ArticleSearchForm extends \yii\base\Model
{
    public $title;
    
    public function rules() {
       return [
           [['title'], 'safe']
       ];
    }
    
    public function search()
    {
        $query = Article::find()
                ->innerJoinWith('content')
                ->select([
                    Article::tableName() . '.article_id',
                    'public_at',
                    'sort',
                    'title',
                ])
                ->where(['public' => true])
                ->andFilterWhere(['like', 'title', $this->title]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => ['sort' => SORT_ASC],
                'attributes' => [
                    'article_id',
                    'public_at',
                    'sort',
                    'title',
                ]
            ]
        ]);
        
        return $dataProvider;
    }
}

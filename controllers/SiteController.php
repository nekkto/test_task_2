<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Article;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchForm = new \app\models\forms\ArticleSearchForm();
        
        $searchForm->load(\Yii::$app->request->get());
        //var_dump($searchForm->attributes); exit;
        
        return $this->render('index', ['searchForm' => $searchForm]);
    }
    
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionViewArticle($article_id)
    {
        $model = Article::findOne(['article_id' => $article_id]);
        
        //var_dump($model); exit;
        
        return $this->render('view-article', ['model' => $model]);
    }
}
